export default interface ITask {
    id: string;
    title: string;
    status: TaskProgress;
    description: string;
    movements: { name: string, date: Date }[];
    createdAt: Date;
    updatedAt: Date;
}

export enum TaskProgress {
    TO_DO = 'Todo',
    IN_PROGRESS = 'In Progress',
    DONE = 'Done'

}
