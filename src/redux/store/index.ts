import {compose, createStore} from "redux";
import {persistStore} from "redux-persist";
import rootReducer from "../reducers";

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


function configureStore() {
    const store = createStore(
        rootReducer,
        composeEnhancers()
    );
    // @ts-ignore
    const persistor = persistStore(store);
    return {store, persistor};
}

export default configureStore;
