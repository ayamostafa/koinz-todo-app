import ITask, {TaskProgress} from '../../models/ITask';
import {ADD_NEW_TASK, DELETE_TASK, EDIT_TASK} from "../types/types";

export const TaskDefaultState: ITask = {
    id: '',
    title: '',
    description: '',
    status: TaskProgress.TO_DO,
    movements: [],
    createdAt: new Date(),
    updatedAt: new Date()
}

const defaultState: { todos: ITask[] } = {
    todos: []
}
const tasksReducer = (state = defaultState, action: { type: string, payload: ITask }) => {
    switch (action.type) {
        case ADD_NEW_TASK:
            return {todos: [...state.todos, {...TaskDefaultState, ...action.payload}]};
        case EDIT_TASK:
            return {
                todos: state.todos.map(todo => {
                    if (todo.id === action.payload.id) {
                        return {...action.payload, updatedAt: new Date()}
                    }
                    return todo;
                })
            }
        case DELETE_TASK:
            return {todos: state.todos.filter((todo: ITask) => todo.id !== action.payload.id)};
        default:
            return state
    }
}
export default tasksReducer
