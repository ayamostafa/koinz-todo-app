import tasksReducer from './tasks'
import {persistReducer} from "redux-persist";
// @ts-ignore
import storageDB from "redux-persist-indexeddb-storage";

const rootReducer = persistReducer(
    {
        key: "todos",
        storage: storageDB("TodoDB"),
        debug: true
    },
    tasksReducer)

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
