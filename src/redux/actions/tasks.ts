import {ADD_NEW_TASK, DELETE_TASK, EDIT_TASK} from "../types/types";
import ITask from "../../models/ITask";
import {v4 as uuidv4} from 'uuid';

const addNewTask = (title: string = '') => ({
    type: ADD_NEW_TASK,
    payload: {id: uuidv4(), title}
});

const editTask = (task: ITask) => ({
    type: EDIT_TASK,
    payload: task
});

const deleteTask = (id = '') => ({
    type: DELETE_TASK,
    payload: {id}
});

export {
    addNewTask,
    editTask,
    deleteTask
}
