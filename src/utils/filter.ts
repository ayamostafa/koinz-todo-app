import ITask, {TaskProgress} from "../models/ITask";

const statuses = [TaskProgress.TO_DO, TaskProgress.IN_PROGRESS, TaskProgress.DONE];

const getByStatus = (status: TaskProgress, tasks: ITask[]) =>
    tasks.filter(task => task.status === status);

export const statusTasksMap = (tasks: ITask[]) => statuses.reduce(
    (previous, status) => ({
        ...previous,
        [status]: getByStatus(status, tasks)
    }),
    {}
);
