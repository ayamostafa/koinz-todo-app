import {useEffect, useState} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {DragDropContext} from "react-beautiful-dnd";
import ITask, {TaskProgress} from "../../models/ITask";
import {reorderTaskMap} from "../../utils/reorder";
import Column from "../../components/Column/Column";
import {statusTasksMap} from '../../utils/filter';
import {RootState} from "../../redux/reducers";
import {editTask} from "../../redux/actions/tasks";

const ordered = [TaskProgress.TO_DO, TaskProgress.IN_PROGRESS, TaskProgress.DONE];
const Home = () => {
    const ToDoTasks: ITask[] = useSelector((state: RootState) => state.todos);
    const dispatch = useDispatch();
    const [columns, setColumns]: any = useState({});

    useEffect(() => {
        setColumns(statusTasksMap(ToDoTasks));
    }, [ToDoTasks])

    const onDragEnd = (result: any) => {
        // dropped nowhere
        if (!result.destination) {
            return;
        }
        const source = result.source;
        const destination = result.destination;
        // did not move anywhere - can bail early
        if (
            source.droppableId === destination.droppableId &&
            source.index === destination.index
        ) {
            return;
        }
        const todo = columns[result.source.droppableId][result.source.index];
        //change task status and register its movements history
        dispatch(editTask({
            ...todo,
            status: destination.droppableId,
            updatedAt: new Date(),
            movements: [...todo.movements, {name: destination.droppableId, date: new Date()}]
        }));

        const data = reorderTaskMap({
            taskMap: columns,
            source,
            destination
        });
        setColumns({...data.taskMap})
    };

    return (<>
        <h1 className='app-title'>Todo App</h1>
        <div className='columns'>
            <DragDropContext onDragEnd={onDragEnd}>
                {ordered.map((key, index) => (
                    <Column key={key}
                            index={index}
                            title={key}
                            tasks={columns[key] || []}
                            isScrollable={true}
                            isCombineEnabled={false}
                    />
                ))}
            </DragDropContext>
        </div>
    </>)
}

export default Home;
