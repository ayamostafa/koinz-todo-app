import ITask, {TaskProgress} from "../../models/ITask";

const tasks: ITask[] = [
    {
        id: '5fa16387-e115-4b97-a3de-e44cdcdd0fb7',
        title: 'task 1 title',
        description: '',
        status: TaskProgress.TO_DO,
        movements: [],
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: '5fa16387-e115-4b97-a3de-e44cdcdd0fb8',
        title: 'task 2 title',
        description: '',
        status: TaskProgress.IN_PROGRESS,
        movements: [],
        createdAt: new Date(),
        updatedAt: new Date()
    },
    {
        id: '5fa16387-e115-4b97-a3de-e44cdcdd0fb9',
        title: 'task 3 title',
        description: '',
        status: TaskProgress.DONE,
        movements: [],
        createdAt: new Date(),
        updatedAt: new Date()
    }
]
export {tasks}
