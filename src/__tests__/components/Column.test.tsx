import React from 'react';
import {DragDropContext} from "react-beautiful-dnd";
import {Provider} from "react-redux";
import {screen, render, within} from '@testing-library/react';
import {tasks} from "../fixtures/fixtures";
import {TaskProgress} from "../../models/ITask";
import {store} from "../mocks/store";
import Column from "../../components/Column/Column";

const renderColumn = (title: string) => {
    render(<Provider store={store}>
        <DragDropContext onDragEnd={() => {}}>
            <Column title={title} tasks={tasks.filter(task => task.status === title)}/>
        </DragDropContext>
    </Provider>);
};

describe("Column Component", () => {

    it("should render todo column tasks correctly", () => {
        let taskTestId = tasks[0].id; //todo 0 with todo status
        renderColumn(TaskProgress.TO_DO);
        const task = screen.getByTestId(taskTestId);
        expect(task).toBeInTheDocument();
    });

    it("should render in progress column tasks correctly", () => {
        let taskTestId = tasks[1].id; //todo 1 with in progress status
        renderColumn(TaskProgress.IN_PROGRESS);
        const task = screen.getByTestId(taskTestId);
        expect(task).toBeInTheDocument();
    });

    it("should render done column tasks correctly", () => {
        let taskTestId = tasks[2].id; //todo 3 with done status
        renderColumn(TaskProgress.DONE);
        const task = screen.getByTestId(taskTestId);
        expect(task).toBeInTheDocument();
    });

});
