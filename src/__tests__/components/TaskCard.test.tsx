import {render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import TaskCard from "../../components/TaskCard/TaskCard";
import {Provider} from 'react-redux';
import {tasks} from "../fixtures/fixtures";
import {store} from "../mocks/store";

describe('card component', () => {
    it('render "task Title" correctly', () => {
        let taskData = tasks[0];
        render(
            <Provider store={store}>
                <TaskCard {...taskData}/>
            </Provider>
        );

        expect(screen.getByText(taskData.title)).toBeInTheDocument()
    });
})

