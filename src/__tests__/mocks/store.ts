import configureStore from 'redux-mock-store';
import {tasks} from "../fixtures/fixtures";

const initialState = {todos: tasks};
const mockStore = configureStore();

export const store = mockStore(initialState);
