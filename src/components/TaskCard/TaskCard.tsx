import {useState} from "react";
import {useDispatch} from "react-redux";
import {DeleteFilled, QuestionCircleOutlined} from "@ant-design/icons";
import {Popconfirm} from 'antd';
import NewCard from "../NewCard/NewCard";
import TaskDetails from "../TaskDetails/TaskDetails";
import {deleteTask} from "../../redux/actions/tasks";
import ITask from '../../models/ITask';
import './TaskCard.scss';


const text = 'Are you sure to delete this task?';
const maxLength = 100;
const TaskCard = ({id, title}: ITask) => {
    const dispatch = useDispatch();
    const [visibleTaskDetails, setVisibleTaskDetails] = useState(false);

    const deleteHandler = (id: string = '') => {
        dispatch(deleteTask(id))
    }

    const toggleVisibilityHandler: any = (val: boolean) => {
        setVisibleTaskDetails(val);
    }
    return (title === '' ? <NewCard/> : <div className='task-card'>
        <p className='card-title  cursor-pointer' onClick={() => toggleVisibilityHandler(true)}>
            <span>{title.substring(0, maxLength)}{title.length > maxLength && <span>...</span>}</span>

            <span onClick={(e) => e.stopPropagation()}>
            <Popconfirm
                icon={<QuestionCircleOutlined className='text-danger'/>}
                placement="topRight"
                title={text}
                onConfirm={() => deleteHandler(id)}
                okText="Yes"
                className='text-danger'
                cancelText="No"
            >
                <DeleteFilled className='text-danger cursor-pointer'/>
            </Popconfirm>
            </span>
        </p>
        {visibleTaskDetails && <TaskDetails visible={visibleTaskDetails} id={id}
                                            toggleVisibilityHandler={toggleVisibilityHandler}/>}
    </div>)
}

export default TaskCard;
