import {Draggable, Droppable} from "react-beautiful-dnd";
import ITask, {TaskProgress} from '../../models/ITask';
import TaskCard from '../TaskCard/TaskCard';
import {TaskDefaultState} from "../../redux/reducers/tasks";
import './Column.scss';

interface ColumnProps {
    title: string;
    tasks: ITask[];
}

const Column: any = ({title, tasks}: ColumnProps) => {
    return (<div className='column'>
            <p className='title'>{title}</p>
            {title === TaskProgress.TO_DO && <TaskCard {...TaskDefaultState} />}
            <Droppable
                droppableId={title}
                type='TASK'
                ignoreContainerClipping={true}
                isDropDisabled={false}
                isCombineEnabled={false}
            >
                {(dropProvided, dropSnapshot) => (
                    <div ref={dropProvided.innerRef} {...dropProvided.droppableProps}>
                        <div className='body' data-testid={title}>
                            {tasks.map((task: ITask, index: number) => {
                                return (<Draggable
                                        key={task.id}
                                        draggableId={task.id}
                                        index={index}
                                        isDragDisabled={task.status === TaskProgress.DONE}
                                    >
                                        {(dragProvided, dragSnapshot) => (
                                            <div data-testid={task.id} ref={dragProvided.innerRef}
                                                 {...dragProvided.draggableProps}
                                                 {...dragProvided.dragHandleProps}>
                                                <TaskCard key={task.id} {...task}/>
                                            </div>
                                        )}
                                    </Draggable>
                                )
                            })}
                            {dropProvided.placeholder}
                        </div>

                    </div>
                )}
            </Droppable>
        </div>
    )
}

export default Column;
