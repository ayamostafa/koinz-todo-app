import {useRef, useState} from "react";
import {useDispatch} from "react-redux";
import {Button} from 'antd';
import {addNewTask} from "../../redux/actions/tasks";

const NewCard = () => {
    const dispatch = useDispatch();
    const [addNew, setAddNew] = useState(true);
    const newTitleRef = useRef<HTMLTextAreaElement>(null);
    const addNewTaskHandler = (event: any) => {
        if (event.key === 'Enter' || event.type === 'click') {
            dispatch(addNewTask(newTitleRef.current?.value));
            setAddNew(true);
        }
    }
    return <>
        {addNew ?
            <p className='task-card bg-light-gray' onClick={() => setAddNew(false)}>
                + Add Todo
            </p> : <div className='task-card'>
            <textarea ref={newTitleRef}
                      placeholder='Enter title for the todo task...'
                      onKeyPress={event => addNewTaskHandler(event)}
                      className='card-title border-0' rows={2}/>
                <Button type="primary" onClick={event => addNewTaskHandler(event)}>Add Card</Button>
            </div>
        }</>

}
export default NewCard;
