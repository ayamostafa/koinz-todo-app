import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {EditOutlined} from "@ant-design/icons";
import {Button, Modal, List} from 'antd';
import {editTask} from "../../redux/actions/tasks";
import {RootState} from "../../redux/reducers";

interface TaskDetailsProps {
    visible: boolean;
    id: string;
    toggleVisibilityHandler: any
}

const options: any = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
};
let movements;
const TaskDetails = ({
                         visible = false,
                         id = '',
                         toggleVisibilityHandler
                     }: TaskDetailsProps) => {
    const dispatch = useDispatch();
    const todo = useSelector((state: RootState) => state.todos.find(todo => todo.id === id))
    const [titleVal, setTitleVal] = useState('');
    const [descVal, setDescVal] = useState('');
    const [toggleActivities, setToggleActivities] = useState(false);
    const [activitiesData, setActivitiesData] = useState<string[]>([]);

    useEffect(() => {
        if (todo) {
            setTitleVal(todo?.title);
            setDescVal(todo?.description);
            movements = todo.movements.map(move => `This card moved to ${move.name.toLowerCase()} at ${new Date(move.date).toLocaleString('en-US', options)}`)
            setActivitiesData([
                `Created at ${new Date(todo.createdAt).toLocaleString('en-US', options)}`,
                `Last updated at ${new Date(todo.updatedAt).toLocaleString('en-US', options)}`,
                ...movements
            ])
        }
    }, [todo])

    const editTitleTaskHandler = (event: any) => {
        setTitleVal(event.target.value);
    }

    const editDescTaskHandler = (event: any) => {
        setDescVal(event.target.value);
    }

    return <Modal
        title={<textarea value={titleVal}
                         onBlur={() => {
                             if (todo) {
                                 dispatch(editTask({...todo, title: titleVal}))
                             }
                         }}
                         onChange={event => editTitleTaskHandler(event)}
                         className='card-title border-0 w-90p' rows={1}/>}
        visible={visible}
        onOk={() => toggleVisibilityHandler(false)}
        onCancel={() => toggleVisibilityHandler(false)}
    >
        <textarea value={descVal}
                  onBlur={() => {
                      if (todo) {
                          dispatch(editTask({...todo, description: descVal}))
                      }
                  }}
                  onChange={event => editDescTaskHandler(event)}
                  className='card-desc' rows={6}/>

        <Button type="default" className='float-right' onClick={() => setToggleActivities(!toggleActivities)}>Show
            Activities</Button>
        <br/>
        {toggleActivities && <List
            size="small"
            className='activities-list'
            dataSource={activitiesData}
            renderItem={item => <List.Item>
                <EditOutlined/>&nbsp;
                {item}
            </List.Item>}
        />}
    </Modal>
}

export default TaskDetails;
